#-------------------------------------------------------------------------------
#author : Grzegorczyk Patryk
#data : 2021.01
#description : program for finding specific marker in BMP file
#inside program I have used fragments of code provided by Zbigniew Szymanski
#-------------------------------------------------------------------------------
#constants
.eqv BMP_MAX_FILE_SIZE 230454
.eqv MAX_SIZE_ARRAY_BLACK 76800
.eqv BYTES_PER_ROW 960
.eqv PIXELS_PER_ROW 320

.data
.align 4
res:
	.space 2
image:
	.space BMP_MAX_FILE_SIZE
nblack_array:
	.space MAX_SIZE_ARRAY_BLACK
file_name:
	.asciiz "source.bmp"
error_file_name:
	.asciiz "Error occured while trying to open file.\n"
error_wrong_format:
	.asciiz "File has wrong format\n"

.text
.globl main

main:
	jal open_file #opening file
	move $a0, $v0 #moving returned value into argument register
	move $s0, $v0 #storing file handler under s0
	jal read_file #reading file
	#checking if it has bmp file header
	la $a0, image
	jal check_format
	#getting height
	la $a0, image
	jal get_height #getting height of image
	move $s1, $v0 #storing height under s1
	blez $s1, file_format_error #height is negitive, exit program
	#getting offset
	la $a0, image
	jal get_offset #getting offset
	move $s2, $v0 #storing offset under s2
	la $t0, image
	add $t0, $t0, $s2
	move $s2, $t0 #storing first pixel adress under s2
	#if height is positive first row is last
	#and last row is first
	#if height is negative first row is first and last row is last
	move $a0, $s1
	move $a1, $s2
	la $a2, nblack_array
	jal start_loop_over_y_reverse #moving bmp into new array
	#which checks if pixels are black and stores 0 if they are or 1 if they are different color
	move $a0, $s1
	la $a1, nblack_array
	jal start_loop_over_y
	#closing file
	move $a0, $s0
	jal close_file
	#ending program

exit:
	#arguments:
	# none
	li $v0, 10 #quit program
	syscall

#opening file
open_file:
	#arguments:
	# none
	#return: file handle
	#ra won't be modiefied no need to store it on sp
	#opening file
	li $v0, 13 #open file
	la $a0, file_name
	li $a1, 0
	li $a2, 0
	syscall
	bltz $v0, open_file_error #if error occured number will be negative, then we exit program
	jr $ra #return

#file name error
open_file_error:
	#arguments:
	# none
	#return: none
	#displaying error text
	li $v0, 4
	la $a0, error_file_name
	syscall
	j exit #quit program

#reading file
read_file:
	#arguments:
	# a0 - file handler
	#return: none
	#ra won't be modiefied no need to store it on sp
	li $v0, 14 #read file
	#a0 is loaded already
	la $a1, image #space adress
	la $a2, BMP_MAX_FILE_SIZE #max size if file is 320x240
	syscall
	jr $ra #return

#closing file
close_file:
	#arguments:
	# a0 - file handler
	#return: none
	li $v0, 16 #close file
	syscall
	jr $ra #return
		
#file format error
file_format_error:
	#arguments:
	# none
	#return: none
	#displaying error text
	li $v0, 4
	la $a0, error_wrong_format
	syscall
	j exit #quit program
	
#checking if file starts with 42 4D
check_format:
	#arguments:
	# a0 - file start
	#return: none
	lb $t0, ($a0)
	bne $t0, 66, file_format_error #checking 42
	addiu $a0, $a0, 1
	lb $t0, ($a0)
	bne $t0, 77, file_format_error #checking 4D
	jr $ra #return

#get height of bmp image
get_height:
	#arguments:
	# a0 file beginning
	#return: v0 - height
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	add $a0, $a0, 22 #height starts at 22nd position in header
	lw $t0, ($a0) #loading height
	move $v0, $t0 #moving result to return register
	
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return
	
#get offset
get_offset:
	#arguments: 
	# a0 file beginning
	#return: v0 - offset
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	add $a0, $a0, 10 #offset data starts at 10th position in header
	lw $t0, ($a0) #loading offset
	move $v0, $t0 #moving result to return register
	
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

#getting pixel
get_pixel:
	#arguments: 
	# a0 - pixels beginning
	# a1 - x cordinates
	# a2 - y cordinates
	#return: v0 - 0RGB
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	mul $t0, $a2, BYTES_PER_ROW #t0 = y * bytes per row
	
	move $t1, $a1 #t1 = x
	sll $t1, $t1, 1 #t1 = t1 * 2
	add $t1, $t1, $a1 #t1 = t1 + x = 3*x
	
	add $t0, $t0, $t1 #t0 = t0 + t1 = 3y*rows + 3x
	
	add $t0, $t0, $a0 #placing t0 at checked pixel adress
	
	#get color
	lbu $v0, ($t0) #load B
	lbu $t1, 1($t0) #load G
	sll $t1, $t1, 8
	or $v0, $v0, $t1
	lbu $t1, 2($t0) #load R
        sll $t1, $t1, 16
	or $v0, $v0, $t1
 
	
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return
	
#returning 0 if black and 1 if not black
is_not_black:
	#arguments: 
	# a0 - array beginning
	# a1 - x cordinates
	# a2 - y cordinates
	#return: v0 - 0RGB
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	mul $t0, $a2, PIXELS_PER_ROW #t0 = y * pixels per row
	
	move $t1, $a1 #t1 = x
	
	add $t0, $t0, $t1 #t0 = t0 + t1 = y*rows + x
	
	add $t0, $t0, $a0 #placing t0 at checked pixel adress
	
	#get result
	lb $v0, ($t0) #loading result
	
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

#loop over height beginning
start_loop_over_y:
	#arguments: 
	# a0 - height
	# a1 - black / not black array beginning
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	sub $sp, $sp, 4 #push s0
	sw $s0, ($sp)
	sub $sp, $sp, 4 #push s1
	sw $s1, ($sp)
	sub $sp, $sp, 4 #push s2
	sw $s2, ($sp)
	sub $sp, $sp, 4 #push s3
	sw $s3, ($sp)
	
	li $s1, 0 #i = 0
	move $s0, $a0 #moving height to s0
	move $s3, $a1 #moving pixels beginning to s3, as it will be used later as an argument

loop_over_y:
	bgeu $s1, $s0, end_loop_over_y #i <= height
	
	jal start_loop_over_x #iterate over pixels in row
	
	add $s1, $s1, 1 #i++
	j loop_over_y #continue loop
	
end_loop_over_y:
	lw $s3, ($sp) #pop s3
	add $sp, $sp, 4
	lw $s2, ($sp) #pop s2
	add $sp, $sp, 4
	lw $s1, ($sp) #pop s1
	add $sp, $sp, 4
	lw $s0, ($sp) #pop s0
	add $sp, $sp, 4
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return
	
#loop over each pixel in row
start_loop_over_x:
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	li $s2, 0 #i = 0

loop_over_x:
	bgeu $s2, 320, end_loop_over_x #j <= 320
	move $a0, $s3 #a0 = black / not black array beginning
	move $a1, $s2 #a1 = j = x
	move $a2, $s1 #a2 = i = y
	move $a3, $s0 #a3 = height
	jal is_not_black
	beqz $v0, check_pixel #pixel is black
check_pixel_return_point:
	add $s2, $s2, 1 #j++
	j loop_over_x #continue loop
	
end_loop_over_x:
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

#checks if pixel is corner of the marker
#must be called from x loop only
check_pixel:
	#arguments:
	# a0 - array beginning
	# a1 - x
	# a2 - y
	# a3 - height
	sub $sp, $sp, 4 #push s0
	sw $s0, ($sp)
	sub $sp, $sp, 4 #push s1
	sw $s1, ($sp)
	sub $sp, $sp, 4 #push s2
	sw $s2, ($sp)
	sub $sp, $sp, 4 #push s3
	sw $s3, ($sp)
	sub $sp, $sp, 4 #push s4
	sw $s4, ($sp)
	sub $sp, $sp, 4 #push s5
	sw $s5, ($sp)
	
	move $s0, $a3 #storing height
	move $s1, $a1 #storing original x
	move $s2, $a2 #and y
	move $s3, $a0 #and array beginning
	add $a1, $s1, 1 #x of pixel to the right
	bge $a1, 320, check_pixel_if_1_true #if x >= 320
	jal is_not_black
	beqz  $v0, return_from_check_pixel #if x+1 black continue loop
check_pixel_if_1_true:
	move $a1, $s1 #restoring original x
	move $a0, $s3
	add $a2, $s2, 1 #pixel below
	bge $a2, $s0, check_pixel_if_2_true #if y >= height
	jal is_not_black
	beqz $v0, return_from_check_pixel #if y+1 black continue loop
check_pixel_if_2_true:
	#at this point we have potential corner of the marker
	move $a0, $s1 #x
	move $a1, $s2 #y
	move $a2, $s3 #array beginning
	jal while_black_start #checking if it's marker
	move $s4, $v0 #s4 = marker arm length
	bltz $s4, return_from_check_pixel #not marker
	#now we check internal layers
	li $s5, 1 #s5 = 1
loop_check_internal_arms:
	beq $s5, $s4, after_verifying_black_pixels #if i == length of arm we check not black pixels
	#checking diagonal pixel - up, left
	move $a0, $s3 #loading beginning of array argument
	sub $a1, $s1, $s5 #x
	sub $a2, $s2, $s5 #y
	jal is_not_black
	bnez $v0, after_verifying_black_pixels
	
	sub $a0, $s1, $s5 #x
	sub $a1, $s2, $s5 #y
	move $a2, $s3 #array beginning
	sub $a3, $s4, $s5 #length of arm
	add $a3, $a3, 1 #we want to make sure there is one non black pixel at the end
	jal verify_black_arms #check internal part of marker
	beqz $v0, return_from_check_pixel #not marker
	#now only margin checking left to confirm that it is marker
	
	add $s5, $s5, 1 #s5++ / i++
	j loop_check_internal_arms
after_verifying_black_pixels:
	#internal layers are ok, checking margins
	#s5 from previous stage is increased, we should get proper results
	beq $s5, $s4, return_from_check_pixel #s5 == length means that it's square (margin 0)
	sub $a0, $s1, $s5 #x
	sub $a1, $s2, $s5 #y
	move $a2, $s3 #array beginning
	sub $a3, $s4, $s5 #length of arm
	move $v1, $s0 #v1 = image heigth
	jal verify_margin #check margins
	beqz $v0, return_from_check_pixel #internal margin is not ok
	#checking outside margins
	move $v1, $s0 #v1 = image heigth
	add $a0, $s1, 1 #x
	add $a1, $s2, 1 #y
	move $a2, $s3 #array beginning
	move $a3, $s4 #length of arm
	jal verify_margin #check margins
	beqz $v0, return_from_check_pixel #external margin is not ok
	#marker is correct, printing cordinates
	move $a0, $s1 #x
	move $a1, $s2 #y
	jal print_cordinates
return_from_check_pixel:
	lw $s5, ($sp) #pop s5
	add $sp, $sp, 4
	lw $s4, ($sp) #pop s4
	add $sp, $sp, 4
	lw $s3, ($sp) #pop s3
	add $sp, $sp, 4
	lw $s2, ($sp) #pop s2
	add $sp, $sp, 4
	lw $s1, ($sp) #pop s1
	add $sp, $sp, 4
	lw $s0, ($sp) #pop s0
	add $sp, $sp, 4
	j check_pixel_return_point
	
#loop over height beginning
start_loop_over_y_reverse:
	#arguments: 
	# a0 - height
	# a1 - pixels beginning
	# a2 - new array beginning
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	sub $sp, $sp, 4 #push s0
	sw $s0, ($sp)
	sub $sp, $sp, 4 #push s1
	sw $s1, ($sp)
	sub $sp, $sp, 4 #push s2
	sw $s2, ($sp)
	sub $sp, $sp, 4 #push s3
	sw $s3, ($sp)
	
	move $s1, $a0 #i = height
	sub $s1, $s1, 1 #i-- since we count from 0
	move $s0, $a2 #current rewriting position
	move $s3, $a1 #moving pixels beginning to s3, as it will be used later as an argument

loop_over_y_reverse:
	bltz $s1, end_loop_over_y_reverse #i >= 0 to continue loop
	
	jal start_loop_over_x_rewrite #iterate over pixels in row
	
	sub $s1, $s1, 1 #i--
	j loop_over_y_reverse #continue loop
	
end_loop_over_y_reverse:
	lw $s3, ($sp) #pop s3
	add $sp, $sp, 4
	lw $s2, ($sp) #pop s2
	add $sp, $sp, 4
	lw $s1, ($sp) #pop s1
	add $sp, $sp, 4
	lw $s0, ($sp) #pop s0
	add $sp, $sp, 4
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

#loop over each pixel in row
start_loop_over_x_rewrite:
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	li $s2, 0 #i = 0

loop_over_x_rewrite:
	bgeu $s2, 320, end_loop_over_x_rewrite #j <= 320 to continue loop
	move $a0, $s3 #a0 = pixels beginning
	move $a1, $s2 #a1 = j = x
	move $a2, $s1 #a2 = i = y
	jal get_pixel
	move $t0, $zero #t0 = false
	beqz $v0, else_rewrite #black is equal 0
	li $t0, 1 #t0 = true
else_rewrite:
	sb $t0, ($s0) #store byte in array
	add $s2, $s2, 1 #j++
	add $s0, $s0, 1 #current rewriting position++
	j loop_over_x_rewrite #continue loop
	
end_loop_over_x_rewrite:
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

while_black_start:
	#arguments: 
	# a0 - x
	# a1 - y
	# a2 - start of array
	#return: v0 - length of marker arm, negative if incorrect marker
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	sub $sp, $sp, 4 #push s0
	sw $s0, ($sp)
	sub $sp, $sp, 4 #push s1
	sw $s1, ($sp)
	sub $sp, $sp, 4 #push s2
	sw $s2, ($sp)
	sub $sp, $sp, 4 #push s3
	sw $s3, ($sp)
	sub $sp, $sp, 4 #push s4
	sw $s4, ($sp)
	
	move $s0, $a0 #s0 = x
	move $s1, $a1 #s1 = y
	li $s2, 0 #marker length = 0
	move $a2, $s3 #s3 = array start
	

while_black:
	move $a0, $s3 #load beginning of array
	sub $a1, $s0, $s2 #checking x-length pixel (on the left)
	li $v0, 1 #in case if x-lenght < 0
	bltz $a1, after_cmp_1
	move $a2, $s1 #loading y
	jal is_not_black
after_cmp_1:
	move $s4, $v0 #storing result

	li $v0, 1 #in case if y-lenght < 0
	move $a0, $s3 #load beginning of array
	move $a1, $s0 #loading x
	sub $a2, $s1, $s2 #checking y-length pixel (above)
	bltz $a2, after_cmp_2
	jal is_not_black

after_cmp_2:
	move $t0, $v0 #storing result
	li, $v0, -1 #result in case if not marker
	bne $t0, $s4, while_black_end #pixels do not match, not marker
	move $v0, $s2 #load result in case if pixels are not black
	bnez $t0, while_black_end #branch if it's end of marker
	add $s2, $s2, 1 #length of marker ++
	j while_black

while_black_end:
	lw $s4, ($sp) #pop s4
	add $sp, $sp, 4
	lw $s3, ($sp) #pop s3
	add $sp, $sp, 4
	lw $s2, ($sp) #pop s2
	add $sp, $sp, 4
	lw $s1, ($sp) #pop s1
	add $sp, $sp, 4
	lw $s0, ($sp) #pop s0
	add $sp, $sp, 4
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

verify_black_arms:
	#arguments: 
	# a0 - x
	# a1 - y
	# a2 - start of array
	# a3 - length
	#return: v0 - 0 if not correct, 1 if correct
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	sub $sp, $sp, 4 #push s0
	sw $s0, ($sp)
	sub $sp, $sp, 4 #push s1
	sw $s1, ($sp)
	sub $sp, $sp, 4 #push s2
	sw $s2, ($sp)
	sub $sp, $sp, 4 #push s3
	sw $s3, ($sp)
	sub $sp, $sp, 4 #push s4
	sw $s4, ($sp)
	sub $sp, $sp, 4 #push s5
	sw $s5, ($sp)
	
	move $s0, $a0 #s0 = x
	move $s1, $a1 #s1 = y
	li $s2, 0 #i = 0
	move $s3, $a3 #s3 = length
	move $s5, $a2
	
loop_verify_black_arms:
	li $v0, 1 #if last loop then it is result
	bge $s2, $s3, verify_black_arms_end #i < length
	move $a0, $s5 #load beginning of array
	sub $a1, $s0, $s2 #checking x-length pixel (on the left)
	li $v0, 1 #in case if x-lenght < 0
	bltz $a1, after_cmp_1_black_arms
	move $a2, $s1 #loading y
	jal is_not_black
after_cmp_1_black_arms:
	move $s4, $v0 #storing result

	li $v0, 1 #in case if y-lenght < 0
	move $a0, $s5 #load beginning of array
	move $a1, $s0 #loading x
	sub $a2, $s1, $s2 #checking y-length pixel (above)
	bltz $a2, after_cmp_2_black_arms
	jal is_not_black

after_cmp_2_black_arms:
	move $t0, $v0 #storing result
	li, $v0, 0 #result in case if not marker
	bne $t0, $s4, verify_black_arms_end #pixels do not match, not marker
	li $t1, 1 #not black
	sub $t2, $s3, 1
	beq $s2, $t2, black_arms_check_result
	li $t1, 0 #black
black_arms_check_result:
	bne $t1, $t0, verify_black_arms_end #pixels not as expected exit returning 0
	add $s2, $s2, 1 #i++
	j loop_verify_black_arms
	
verify_black_arms_end:
	lw $s5, ($sp) #pop s5
	add $sp, $sp, 4
	lw $s4, ($sp) #pop s4
	add $sp, $sp, 4
	lw $s3, ($sp) #pop s3
	add $sp, $sp, 4
	lw $s2, ($sp) #pop s2
	add $sp, $sp, 4
	lw $s1, ($sp) #pop s1
	add $sp, $sp, 4
	lw $s0, ($sp) #pop s0
	add $sp, $sp, 4
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

verify_margin:
	#arguments: 
	# a0 - x
	# a1 - y
	# a2 - start of array
	# a3 - length
	# v1 - image height
	#return: v0 - 0 if not correct, 1 if correct
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	sub $sp, $sp, 4 #push s0
	sw $s0, ($sp)
	sub $sp, $sp, 4 #push s1
	sw $s1, ($sp)
	sub $sp, $sp, 4 #push s2
	sw $s2, ($sp)
	sub $sp, $sp, 4 #push s3
	sw $s3, ($sp)
	sub $sp, $sp, 4 #push s4
	sw $s4, ($sp)
	sub $sp, $sp, 4 #push s5
	sw $s5, ($sp)
	
	move $s0, $a0 #s0 = x
	move $s1, $a1 #s1 = y
	li $s2, 0 #i = 0
	move $s3, $a3 #s3 = length
	move $s5, $a2
	
loop_verify_margin:
	li $v0, 1 #if last loop then it is result
	bge $s2, $s3, verify_margin_end #i < length
	move $a0, $s5 #load beginning of array
	sub $a1, $s0, $s2 #checking x-length pixel (on the left)
	li $v0, 1 #in case if x-lenght < 0 or 320 <= x-length or y >= height
	move $a2, $s1 #loading y
	bge $a2, $v1, after_cmp_1_margin
	bltz $a1, after_cmp_1_margin
	bge $a1, 320, after_cmp_1_margin
	jal is_not_black
after_cmp_1_margin:
	move $s4, $v0 #storing result

	li $v0, 1 #in case if y-lenght < 0 or heigth <= y-length or x >= 320
	move $a0, $s5 #load beginning of array
	move $a1, $s0 #loading x
	sub $a2, $s1, $s2 #checking y-length pixel (above)
	bge $a1, 320, after_cmp_2_margin
	bltz $a2, after_cmp_2_margin
	bge $a2, $v1, after_cmp_2_margin
	jal is_not_black

after_cmp_2_margin:
	move $t0, $v0 #storing result
	li, $v0, 0 #result in case if not marker
	bne $t0, $s4, verify_margin_end #pixels do not match, not marker

	beqz $t0, verify_margin_end #pixels are black exit returning 0
	add $s2, $s2, 1 #i++
	j loop_verify_margin
	
verify_margin_end:
	lw $s5, ($sp) #pop s5
	add $sp, $sp, 4
	lw $s4, ($sp) #pop s4
	add $sp, $sp, 4
	lw $s3, ($sp) #pop s3
	add $sp, $sp, 4
	lw $s2, ($sp) #pop s2
	add $sp, $sp, 4
	lw $s1, ($sp) #pop s1
	add $sp, $sp, 4
	lw $s0, ($sp) #pop s0
	add $sp, $sp, 4
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return

print_cordinates:
	#arguments: 
	# a0 - x
	# a1 - y
	sub $sp, $sp, 4 #push ra
	sw $ra, ($sp)
	
	li $v0, 1
	syscall
	li $v0, 11
	li $a0, 44 #44 in ascii is comma
	syscall
	li $v0, 1
	move $a0, $a1
	syscall
	li $v0, 11
	li $a0, 10 #10 in ascii is LF
	syscall
	
	lw $ra, ($sp) #pop ra
	add $sp, $sp, 4
	jr $ra #return
